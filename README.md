# Sign-up Form

Use the live sign-up form at [https://yoadrian.gitlab.io/sign-up-form/](https://yoadrian.gitlab.io/sign-up-form/)

## Project learning-points
- I created the form using position `relative` for input containers and position `absolute` for labels so that they appear to be inside the input field
- I used a combination of of `:focus`, `:valid`, `:invalid`, and `:placehoder-shown` pseudo-classes to create an interactive form-validation UX, with different colors for the input background, label text, and input caret
- I used the `var()` custom property for valid/ invalid colors ~~and the `min()` CSS function~~
- I used regular expressions for input fields to define a pattern of constraints
- I created a JavaScript function for password confirmation, and altered styles to visually show through color and text when the passwords match
- I used the same script to create a constraint, disabling the `Create Account` button until the passwords match
- I opted for input `autofocus` as a feature for this assignment, but I would leave this out for accessibility reasons in real-world applications
- I used the Meyer Reset learned prior to the assignment